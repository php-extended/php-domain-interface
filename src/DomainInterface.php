<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-domain-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Domain;

use InvalidArgumentException;
use LengthException;
use Stringable;

/**
 * DomainInterface interface file.
 * 
 * This interface represents a single domain name.
 * 
 * @author Anastaszor
 */
interface DomainInterface extends Stringable
{
	
	/**
	 * Gets a new domain name with the part appended at the beginning of the
	 * domain name.
	 *
	 * @param string $part
	 * @return DomainInterface
	 * @throws InvalidArgumentException if the part is invalid
	 * @throws LengthException if the added part exceeds the allowed length
	 *                         of a domain (255 chars total per RFC 1034)
	 */
	public function append(string $part) : DomainInterface;
	
	/**
	 * Gets the depths of the domain name, effectively counting the number
	 * of dots in the domain.
	 * 
	 * @return integer
	 */
	public function getDepths() : int;
	
	/**
	 * Gets the length (in octets) of this domain name, effectively counting
	 * the number of characters in the domain.
	 * 
	 * @return integer
	 */
	public function getLength() : int;
	
	/**
	 * Gets a new domain name with the more specific part stripped of this
	 * domain name.
	 * 
	 * @return DomainInterface
	 */
	public function getParent() : DomainInterface;
	
	/**
	 * Gets a string representation of this domain that is compliant with 
	 * RFC 1034.
	 * 
	 * @return string
	 */
	public function getCanonicalRepresentation() : string;
	
	/**
	 * Gets whether this domain name is empty.
	 *
	 * @return bool
	 */
	public function isEmpty() : bool;
	
}
